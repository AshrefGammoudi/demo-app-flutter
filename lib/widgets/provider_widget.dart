import 'package:demo/resources/auth.dart';
import 'package:flutter/material.dart';
//import 'package:socially/resources/repository.dart';

class Provider extends InheritedWidget {
  final Auth auth;

  Provider({Key key, Widget child, this.auth}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static Provider of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(Provider) as Provider);
}