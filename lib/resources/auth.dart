import 'dart:async';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:demo/models/user.dart';

abstract class BaseAuth {

}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  //DEB JDID
  final Firestore _firestore = Firestore.instance;
  User user;

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  StorageReference _storageReference;

     Stream<String> get onAuthStateChanged => _firebaseAuth.onAuthStateChanged.map(
        (FirebaseUser user) => user?.uid,
      );

  Future<void> addDataToDb(FirebaseUser currentUser) async {
    print("Inside addDataToDb Method");

    _firestore
        .collection("display_names")
        .document(currentUser.displayName)
        .setData({'displayName': currentUser.displayName});

    user = User(
        uid: currentUser.uid,
        email: currentUser.email,
        displayName: currentUser.displayName,
        photoUrl: currentUser.photoUrl,
        );


    return _firestore
        .collection("users")
        .document(currentUser.uid)
        .setData(user.toMap(user));
  }
    
    Future<bool> authenticateUser(FirebaseUser user) async {
    print("Inside authenticateUser");
    final QuerySnapshot result = await _firestore
        .collection("users")
        .where("email", isEqualTo: user.email)
        .getDocuments();

    final List<DocumentSnapshot> docs = result.documents;

    if (docs.length == 0) {
      return true;
    } else {
      return false;
    }
  }

  Future<FirebaseUser> gSignIn() async {
    GoogleSignInAccount _signInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication _signInAuthentication =
        await _signInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: _signInAuthentication.accessToken,
      idToken: _signInAuthentication.idToken,
    );

    final FirebaseUser user = (await _firebaseAuth.signInWithCredential(credential)).user;
    return user;
  }

//END JDID
  //@override
  Future<FirebaseUser> signIn(String email, String password) async {
    final FirebaseUser user = (await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password)).user;
    return user;
  }

  //@override
  Future<FirebaseUser> createUser(String email, String password, String name, String age) async {
    final FirebaseUser user = (await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password)).user;
    return user/*?.uid*/;
  }
//DEB JDID
    Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser currentUser = await _firebaseAuth.currentUser();
    print("EMAIL ID : ${currentUser.email}");
    return currentUser;
  }

//END JDID


  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

/* ******************************************************************************************************************** */

}
