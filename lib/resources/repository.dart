import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:demo/models/user.dart';
import 'package:demo/resources/auth.dart';

class Repository {

  final _firebaseProvider = Auth();

  Future<FirebaseUser> signIn(String email, String password) => _firebaseProvider.signIn(email, password);

  Future<FirebaseUser> createUser(String email, String password, String name, String age) => _firebaseProvider.createUser(email, password, name, age);

  Future<void> addDataToDb(FirebaseUser user) => _firebaseProvider.addDataToDb(user);
  
  Future<bool> authenticateUser(FirebaseUser user) => _firebaseProvider.authenticateUser(user);

  Future<FirebaseUser> getCurrentUser() => _firebaseProvider.getCurrentUser();

  Future<FirebaseUser> gSignIn() => _firebaseProvider.gSignIn();

  Future<void> signOut() => _firebaseProvider.signOut();

}