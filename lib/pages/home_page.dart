import 'package:flutter/material.dart';
import 'package:demo/resources/auth.dart';
import 'package:demo/widgets/provider_widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home',style: TextStyle(fontSize: 20.0, color: Colors.black)),
        actions: <Widget>[
          IconButton(
            icon: ImageIcon(AssetImage("assets/ico/logout.png"),color: Colors.black,),
            onPressed: () async {
              try {
                Auth auth = Provider.of(context).auth;
                await auth.signOut();
                print("Signed Out!");
              } catch (e) {
                print (e);
              }
            },
          )
        ],
        backgroundColor: Colors.white,
        elevation: 1,
        shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
        bottom: Radius.circular(30),
       ),
      )
      ),
      body: Container(
        child: Center(
          child: Text('Welcome ', style: TextStyle(fontSize: 32.0))
          ),
      ),
    );
  }
}
