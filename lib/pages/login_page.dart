import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:demo/resources/repository.dart';
import 'package:demo/widgets/provider_widget.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';


enum AuthFormType { signIn, signUp }

class SignUpView extends StatefulWidget {
  final AuthFormType authFormType;

  SignUpView({Key key, @required this.authFormType}) : super(key: key);

  @override
  _SignUpViewState createState() =>
      _SignUpViewState(authFormType: this.authFormType);
}

class _SignUpViewState extends State<SignUpView> {
  AuthFormType authFormType;
  Repository _repository;
  
  @override
  void initState() {
    super.initState();
    _repository = new Repository();
  }

  _SignUpViewState({this.authFormType});

  final formKey = GlobalKey<FormState>();
  String _email, _password, _name, _age;

  void switchFormState(String state) {
    formKey.currentState.reset();
    if (state == "signUp") {
      setState(() {
        authFormType = AuthFormType.signUp;
      });
    } else {
      setState(() {
        authFormType = AuthFormType.signIn;
      });
    }
  }

  void submit() async {

    final form = formKey.currentState;
    form.save();
    try {

      final auth = Provider.of(context).auth;
      if(authFormType == AuthFormType.signIn) {

        //String uid = await auth.signIn(_email, _password);
        //Navigator.of(context).pushReplacementNamed('/home');
         //print("Signed In with ID $uid");
         //Navigator.of(context).pushReplacementNamed('/home');
       /* _repository*/auth.signIn(_email, _password).then((user){
           if (user != null) {
          authenticateUser(user);
        }else{
          print('error');
        }
        });

      } else {
        /* _repository*/auth.createUser(_email, _password, _name,_age).then((user) {
        if (user != null) {
          authenticateUser(user);
        }
         });
        /*String uid = await auth.createUser(_email, _password, _name,_age);
        print("Signed up with New ID $uid");
        Navigator.of(context).pushReplacementNamed('/home');*/
      }
    } catch (e) {
      print (e);
    }
  }

    void authenticateUser(FirebaseUser user) {
    print("Inside Login Screen -> authenticateUser");
    _repository.authenticateUser(user).then((value) {
      if (value) {
        print("VALUE : $value");
        print("INSIDE IF");
        _repository.addDataToDb(user).then((value) {
          Navigator.of(context).pushReplacementNamed('/home');
        });
      } else {
        print("INSIDE ELSE");
        Navigator.of(context).pushReplacementNamed('/home');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height;

    return Scaffold(
      body:Container(
       decoration:BoxDecoration(
        gradient: LinearGradient(colors: [Color(0xff6D0EB5),Color(0xff4059F1)],
        begin: Alignment.bottomRight,
        end: Alignment.centerLeft),),
        padding: EdgeInsets.all(16.0),
        height: _height,
        width: _width,
        child: Center(
          child: Stack(
            children: <Widget>[
             
              ClipPath(
                clipper: RoundedDiagonalPathClipper(),
                child: Container(
                  height: 500,
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(40.0)),
                    color: Colors.white,
                  ),
          child: Column(
            children: <Widget>[
              SizedBox(height: _height * 0.05),
              buildHeaderText(),
              SizedBox(height: _height * 0.05),
              Padding(
                padding: const EdgeInsets.only(top:10.0,bottom: 20.0,right: 20.0,left: 20.0,),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: buildInputs() + buildButtons(),
                  ),
                ),
              ),
              // only lel login
              Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    SizedBox(height: 10.0,),
                      //forgot psd
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(padding: EdgeInsets.only(right: 20.0),
                            child: Text("Forgot Password",
                              style: TextStyle(color: Colors.black45),
                            )
                          )
                        ],
                      ),

                    ],
                  ),
            ],
          ),
           ),
           ),
          ],
         ),
        ),
      ),
    );
  }

  AutoSizeText buildHeaderText() {
    String _headerText;
    if (authFormType == AuthFormType.signUp) {
      _headerText = "Create New Account";
    } else {
      _headerText = "Sign In";
    }
    return AutoSizeText(
      _headerText,
      maxLines: 1,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 35,
        color: Colors.white,
      ),
    );
  }

  List<Widget> buildInputs() {
    List<Widget> textFields = [];

    // if were in the sign up state add name and age
    if (authFormType == AuthFormType.signUp) {
      textFields.add(
        TextFormField(
          style: TextStyle(fontSize: 15.0,color: Colors.blue),
          decoration: buildSignUpInputDecoration("username..."),
           validator: (value){
          return value.isEmpty ? 'username required': null;
        },
          onSaved: (value) => _name = value.trim(),
        ),
      );
      textFields.add(SizedBox(height: 10));
      textFields.add(
        TextFormField(
          style: TextStyle(fontSize: 15.0,color: Colors.blue),
          decoration: buildSignUpInputDecoration("age..."),
           validator: (value){
          return value.isEmpty ? 'age required': null;
        },
          onSaved: (value) => _age = value,
        ),
      );
      textFields.add(SizedBox(height: 15));
    }

    // add email & password
    textFields.add(
      TextFormField(
        style: TextStyle(fontSize: 15.0,color: Colors.blue),
        decoration: buildSignUpInputDecoration("email..."),
        validator: (value){
          return value.isEmpty ? 'email required': null;
        },
        onSaved: (value) => _email = value.trim(),
      ),
    );
    textFields.add(SizedBox(height: 15));
    textFields.add(
      TextFormField(
        style: TextStyle(fontSize: 15.0,color: Colors.blue),
        decoration: buildSignUpInputDecoration("password..."),
         validator: (value){
          return value.isEmpty ? 'password required': null;
        },
        obscureText: true,
        onSaved: (value) => _password = value,
      ),
    );
    textFields.add(SizedBox(height: 10));

    return textFields;
  }

  InputDecoration buildSignUpInputDecoration(String hint) {
    return InputDecoration(
      hintText: hint,
      filled: true,
      fillColor: Colors.white,
      focusColor: Colors.blue,
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 0.0)),
      contentPadding:
          const EdgeInsets.only(left: 14.0, bottom: 10.0, top: 10.0),
    );
  }

  List<Widget> buildButtons() {
    String _switchButtonText, _newFormState, _submitButtonText;

    if (authFormType == AuthFormType.signIn) {
      _switchButtonText = "Create New Account";
      _newFormState = "signUp";
      _submitButtonText = "Let me in";

    } else {
      _switchButtonText = "Have an Account? LogIn";
      _newFormState = "signIn";
      _submitButtonText = "Create Account";
    }

    return [
      Container(
        width: MediaQuery.of(context).size.width * 0.7,
        child: RaisedButton(
            shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0)),
            color: Colors.blue,
            textColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                _submitButtonText,
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            ),
            onPressed: submit,
        ),
      ),
      FlatButton(
        child: Text(
          _switchButtonText,
          style: TextStyle(color: Colors.black),
        ),
        onPressed: () {
          switchFormState(_newFormState);
        },
      )
    ];
  }
}